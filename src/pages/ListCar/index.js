import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import CarListComponnent from '../../components/CarListComponnent'

const ListCar = () => {
  return(
    <View style={{ backgroundColor: '#FFFFFF'}}>
        <ScrollView style={{ marginTop: 10 }}>
            <CarListComponnent/>
            <CarListComponnent/>
            <CarListComponnent/>
            <CarListComponnent/>
            <CarListComponnent/>
            <CarListComponnent/>
            <CarListComponnent/>
            <CarListComponnent/>
        </ScrollView>            
    </View>
  );
}

export default ListCar

const styles = StyleSheet.create({})