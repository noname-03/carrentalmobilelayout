import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native'
import React, { useEffect } from 'react'
import {Car } from '../../assets'

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('MainApp')
        }, 3000)
    }, [navigation])
    return (
        <View style={styles.background}>
            <Text style={styles.text}>BCR</Text>
            <Text style={styles.text}>Binar Car Rental</Text>
            <View style={styles.bgimage}>
            </View>
            <Image source={Car} style={styles.image} />
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#091B6F',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        top: 100,
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    },
    image: {
        top: 200,
        width: 359,
        height: 208
    },
    bgimage: {
        top: 420,
        width: 400,
        height: 130,
        backgroundColor: '#D3D9FD',
        borderTopStartRadius: 60,
        overflow: 'hidden'
    }
})