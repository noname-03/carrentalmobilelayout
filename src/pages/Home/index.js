import { StyleSheet, Text, View, Image, Dimensions, ImageBackground, Button, ScrollView } from 'react-native'
import React from 'react'
import { Avatar, BgHeader } from "../../assets";
import { roundToNearestPixel } from 'react-native/Libraries/Utilities/PixelRatio';
import CarListComponnent from '../../components/CarListComponnent';
import { Box, Truck, Camera, Key } from "../../assets/icons";
import IconMenu from '../../components/IconMenu';

const widthDimension = Dimensions.get('window').width
const heightDimension = Dimensions.get('window').height

const Home = () => {
  return (
    <View style={styles.page}>
      <ScrollView>
        <View style={styles.header}>
          <View style={styles.row}>
            <View style={styles.colomn}>
              <Text>Hi, Nama</Text>
              <Text style={{ paddingTop: 4, fontWeight: 'bold' }}>Your Location</Text>
            </View>
            <Image source={Avatar} style={styles.avatar} />
          </View>
          <View style={styles.headItem}>
            <ImageBackground source={BgHeader} style={styles.image}>
              <Text style={{ color: 'white', fontSize: 16, width: 200, paddingTop: 20, paddingLeft: 20 }}>Sewa Mobil Berkualitas di Kawasanmu</Text>
              <View style={{ paddingTop: 16, width: 140, paddingLeft: 20 }}>
                <Button title="Sewa Mobil" color="#5CB85F" />
              </View>
            </ImageBackground>
          </View>
        </View>
        <View style={{ paddingTop: 60 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <IconMenu
              title="Sewa Mobil"
              url={Truck}
            />
            <IconMenu
              title="Oleh - oleh"
              url={Box}
            />
            <IconMenu
              title="Penginapan"
              url={Key}
            />
            <IconMenu
              title="Wisata"
              url={Camera}
            />
          </View>
        </View>
        <Text style={{ paddingTop: 16, fontWeight: 'bold', paddingLeft: 20 }}>Daftar Mobil Pilihan</Text>
        <View style={{paddingTop: 16}}>
          <CarListComponnent />
          <CarListComponnent />
          <CarListComponnent />
          <CarListComponnent />
          <CarListComponnent />
          <CarListComponnent />
          <CarListComponnent />
        </View>

      </ScrollView>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  page: {
    flex: 1
  },
  header: {
    paddingTop: 10,
    backgroundColor: '#D3D9FD',
    width: widthDimension,
    height: heightDimension * 0.25
  },
  row: {
    paddingLeft: 10,
    flexDirection: 'row'
  },
  colomn: {
    flexDirection: 'column'
  },
  avatar: {
    left: 240,
    width: 40,
    height: 40
  },
  headItem: {
    alignItems: 'center',
    paddingTop: 50
  },
  image: {
    width: 328,
    height: 140
  }
})