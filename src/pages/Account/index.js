import { StyleSheet, Text, View, Button, Image, Dimensions } from 'react-native'
import React from 'react'
import { BgAccount } from "../../assets/images";

const Account = () => {
  return (
    <View style={styles.viewIndex}>
      <View>
        <Image style={styles.Image} source={BgAccount} />
      </View>
      <View>
        <Text style={styles.text}>
          Upps kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR lebih mudah
        </Text>
      </View>
      <View style={styles.topSize}>
        <Button title='Register' color="#5CB85F" style={styles.btnRegister} />
      </View>
    </View>
  )
}

export default Account


const widthDimension = Dimensions.get('window').width
const heightDimension = Dimensions.get('window').height
const styles = StyleSheet.create({
  viewIndex: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -20
  },
  Image: {
    width: widthDimension,
    height: heightDimension * 0.25,
    left: 0,
    top: 0,
  },
  text: {
    fontFamily: 'Helvetica',
    fontSize: 14,
    alignItems: 'center',
  },
  topSize: {
    paddingTop: 20,
  }
})