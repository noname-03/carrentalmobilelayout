import IconHome  from "./home.svg";
import IconHomeActive  from "./homeAc.svg";
import IconList  from "./list.svg";
import IconListActive  from "./listAc.svg";
import IconUser  from "./user.svg";
import IconUserActive  from "./userAc.svg";
import Box  from "./fi_box.png";
import Camera  from "./fi_camera.png";
import Key  from "./fi_key.png";
import Truck  from "./fi_truck.png";

export { IconHome, IconHomeActive, IconList, IconListActive, IconUser, IconUserActive, Box, Key, Camera, Truck }