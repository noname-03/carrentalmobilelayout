import BgSplash from './bgsplash.png'
import Bg from './bg.png'
import Car from './car.png'
import Avatar from './avatar.png'
import BgHeader from './BgHeader.png'
import BgAccount from './bgAccount.png'
import CarList from './CarList.png'

export { BgSplash , Car, Bg, Avatar, BgHeader, BgAccount, CarList }